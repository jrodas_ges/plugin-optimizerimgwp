<?php
/**
* Plugin Name:Optimizar Imagenes 
* Description:Plugin para Optimizar y Cachear Imagenes.
* Version: 1.0.0
* Author: GESDEV
* Author URI: http://www.galileo.edu 
*/
include('OptimizerImg.php');
function img_admin_menu() {
          add_options_page('Optimizar Imagenes','Optmizar Imagenes','manage_options','img-galileo','img_page');
}

/*
 * Compresion de imagen al cargar  en biblioteca de wordpress
 */
add_filter('add_attachment','optimizer_attach',$_GET['post_id']);
function optimizer_attach($post) {
    $img=get_post($post);
    optimizerImg($img->ID);
    return $post;
}
function optimizerImg($img){
    $parsed = parse_url( wp_get_attachment_url($img) );
    $path   = get_home_path() . dirname( $parsed [ 'path' ] ) . '/' . rawurlencode( basename( $parsed[ 'path' ] ) );
    $path   = str_replace('//','/',$path);
    // Soporte a sub sitios
    global $blog_id;
    $blog = absint($blog_id);
    if($blog > 1){
        $blogName = strtolower(get_bloginfo('name'));
        $path_blog = 'wp-content/blogs.dir/' . $blog ;
        $path = str_replace($blogName,$path_blog,$path);
    }
    /*Comprime las imagenes*/
    if(is_readable($path)){
        $name = basename((string)$path);
        $type = explode(".", $name);
        $type = end($type);
        if($type=='jpg' or ($type=='png' or $type=='gif')){
            $optimizer = new OptimizerImg();
            $file= (string) $path;
            $optimizer->loadImage($file);
            $optimizer->save($file,90);
            unset($thum);
        }
    }
}
?>
