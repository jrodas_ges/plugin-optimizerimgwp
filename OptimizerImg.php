<?php
class OptimizerImg {

    private $image;
    private $type;
    private $width;
    private $height;

    public function loadImage($name) {
        $info = getimagesize($name);
        $this->width = $info[0];
        $this->height = $info[1];
        $this->type = $info[2];

        switch($this->type){
            case IMAGETYPE_JPEG:
                $this->image = imagecreatefromjpeg($name);
                break;
            case IMAGETYPE_GIF:
                $this->image = imagecreatefromgif($name);
                break;
            case IMAGETYPE_PNG:
                $this->image = imagecreatefrompng($name);
                break;
        }
    }

    public function save($name, $quality = 100) {
        switch($this->type){
            case IMAGETYPE_JPEG:
                imagejpeg($this->image, $name, $quality);
                break;
            case IMAGETYPE_GIF:
                imagegif($this->image, $name);
                break;
            case IMAGETYPE_PNG:
                $pngquality = floor(($quality - 10) / 10);
                imagepng($this->image, $name, $pngquality);
                break;
        }
    }
}
?>